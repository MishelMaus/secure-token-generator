# Secure token generator

Generates tokens of chosen length with [Python secrets](https://docs.python.org/3/library/secrets.html) module

## Requirements

See `requirements.txt`

## Usage

1. Launch `main.py`
2. The API is available at `http://localhost:8080/generate-token/<length_bytes:int>`
3. Make a GET request to it

## Example
### Request

`http://localhost:8080/generate-token/10`

### Response
Each byte is encoded with two hexadecimal characters

    {
        "length_bytes": 10,
        "token": "8991d68c5a20299a3489"
    }
