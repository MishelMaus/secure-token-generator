from bottle import get, run

import secrets


@get("/generate-token/<length_bytes:int>")
def generate_token(length_bytes):

    return {
        'length_bytes': length_bytes,
        'token': secrets.token_hex(length_bytes)
    }


run(host='localhost', port=8080)
